<?php

namespace App\Http\Controllers;

use App\Mail\Welcome;
use Illuminate\Http\Request;
use App\Contact;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('contact.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name=$request->inputName;
        $mail=$request->inputEmail;
        $text=$request->inputText;

        Contact::create([
            "name" => $name,
            "mail" => $mail,
            "message" => $text
        ]);

        // het bericht wat verzonden word. Dit verwijst naar de pagina "welcome.blade.php"
        \Mail::to($mail)->send(new Welcome);

        $message='Your question has been send succesfully!';
        return redirect('contact')->with('message', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @parant  $id
     * @return \Illuminate\Http\Response
     */

    public function showAll()
    {
        $allcontacts = Contact::all();
        return view ('contact.allcontacts')->with(["allcontacts" => $allcontacts]);
    }
}
