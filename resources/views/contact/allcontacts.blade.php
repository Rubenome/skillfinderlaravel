 resources/views/admin/dashboard.blade.php

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')

@stop

@section('content')
<h1>All contacts</h1>
    @foreach($allcontacts as $row)

       <div class="col-md-3 col-sm-6 col-xs-12">
           <div class="info-box">
               <span class="info-box-icon bg-aqua"><i class="fa fa-envelope-o"></i></span>

               <div class="info-box-content">
                   <span class="info-box-text">{{$row->name }}</span>
                   <span class="text-gray">{{$row->mail}}</span><br />
                   <span class="">{{$row->message}}</span>
               </div>
               <!-- /.info-box-content -->
           </div>
           <!-- /.info-box -->
       </div>
    @endforeach
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
