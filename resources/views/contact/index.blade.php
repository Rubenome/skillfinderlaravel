{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')

@stop

@section('content')
    <?php
//        if(isset($_REQUEST['inputName'])){
//            $name=$_REQUEST['inputName'];
//        }
//
//        if(isset($_REQUEST['inputEmail'])){
//            $mail=$_REQUEST['inputEmail'];
//        }
//
//        if(isset($_REQUEST['inputText'])){
//            $text=$_REQUEST['inputText'];
//        }
//
//        if(isset($name) && isset($mail) && isset($text)){
//            echo($name . ', ' . $mail . ', ' . $text);
//

    ?>
    <section id="contact" class="content-section text-left">
        <div class="contact-section">
            <div class="container">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        {{ Session::get('message') }}
                    </div>
                    @endif
                    <h2>Contact Us.</h2>
                    <div class="row">
                        <div class="col-md-8 col-md-offset-0">
                            <form class="form-horizontal" action="{{ route('contact.store') }}" method="post">
                                <div class="form-group">
                                    <label for="inputName">Name</label>
                                    <input type="text" class="form-control" name="inputName" placeholder="Piet Mattenklopper" required autofocus>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail">E-mail</label>
                                    <input type="email" class="form-control" name="inputEmail" placeholder="p.matten@klopper.com" required>
                                </div>
                                <div class="form-group">
                                    <label for="inputText">Your Message</label>
                                    <textarea  class="form-control" name="inputText" placeholder="Your message" required></textarea>
                                    {{--Het CSRF_token zorgt ervoor dat je niet op een andere manier op de pagina kan.--}}
                                </div>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" class="btn btn-default">Send Message</button>
                            </form>

                            <hr>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    @stop

    @section('css')
        <link rel="stylesheet" href="/css/admin_custom.css">
    @stop

    @section('js')
        <script> console.log('Hi!'); </script>
    @stop