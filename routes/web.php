<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// De routes bepalen waar ze naartoe gelinkt wordt, dus als je bijvoorbeeld naar de /contact pagina gaat, dan
//linkt deze hem naar de contactController.
//Route::get('/tickets', 'ticketcontroller@index');
Route::get('/admin', 'pagescontroller@admin');
//Route::get('/search', 'searchcontroller@index');
//Route::get('/contact', 'contactcontroller@index');
//Route::post('/contact/store','contactcontroller@store')->name('contact.store');
Route::get('allcontacts', 'contactcontroller@showAll')->name('allcontacts');
Route::resource('contact', 'contactcontroller');

//Route::get('/registration', /** @lang text */
//'registercontroller@getRegisterForm');

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
